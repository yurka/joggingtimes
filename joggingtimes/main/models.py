from datetime import timedelta, date

from django.db import models
from django.db import connection
from django.contrib.auth.models import User


class Record(models.Model):

    """
    Jogging time record
    """
    user = models.ForeignKey(User)
    date = models.DateField()
    time = models.TimeField()
    distance = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date']

    @property
    def avg_speed(self):
        time_in_seconds = timedelta(hours=self.time.hour,
                                    minutes=self.time.minute,
                                    seconds=self.time.second).total_seconds()
        return self.distance / time_in_seconds / 1000 * 3600

    @classmethod
    def get_summary(cls, user):
        today = date.today()
        week_start = today - timedelta(days=today.weekday())
        week_end = today + timedelta(days=6 - today.weekday())
        q = """
            SELECT avg(time), avg(distance) 
            FROM main_record 
            WHERE user_id=%s AND date BETWEEN %s AND %s
            """
        cursor = connection.cursor()
        cursor.execute(q, [user.pk, week_start, week_end])
        row = cursor.fetchone()
        avg_distance = float(row[1]) if row[1] else 0
        avg_time = row[0].total_seconds() if row[0] else 0
        if avg_time == 0:
            avg_speed = 0
        else:
            avg_speed = avg_distance / 1000 / avg_time * 3600
        data = {
            'avg_distance': avg_distance,
            'avg_speed': avg_speed,
        }
        return data
