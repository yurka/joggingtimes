from django.core.urlresolvers import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from main.models import Record


class RecordTests(APITestCase):

    fixtures = ['common.json']

    def test_access_noauth(self):
        response = self.client.get(reverse('record-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.post(reverse('record-list'), {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.put(reverse('record-detail', kwargs={'pk': 3}),
                                   {},
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.delete(
            reverse('record-detail', kwargs={'pk': 3}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_record(self):
        """
        Ensure we can create a new record.
        """
        url = reverse('record-list')
        data = {'date': '2015-08-25', 'time': '00:15:00', 'distance': 1024}
        self.client.login(username='user', password='123')
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictContainsSubset(data, response.data)
        self.assertEqual(response.data.get('id'), 4)
        self.assertEqual(response.data.get('user'), 2)

        records = Record.objects.all()
        self.assertEqual(len(records), 4)

    def test_update_record(self):
        """
        Ensure we can update the record.
        """
        url = reverse('record-detail', kwargs={'pk': 3})
        data = {'date': '2015-08-25', 'time': '00:15:00', 'distance': 500}
        self.client.login(username='user', password='123')
        response = self.client.put(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictContainsSubset(data, response.data)
        self.assertEqual(response.data.get('id'), 3)
        self.assertEqual(response.data.get('user'), 2)

        records = Record.objects.all()
        self.assertEqual(len(records), 3)

    def test_delete_record(self):
        """
        Ensure we can delete the record.
        """
        self.client.login(username='user', password='123')

        url = reverse('record-detail', kwargs={'pk': 3})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        records = Record.objects.all()
        self.assertEqual(len(records), 2)

        url = reverse('record-detail', kwargs={'pk': 1})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        records = Record.objects.all()
        self.assertEqual(len(records), 2)

        url = reverse('record-detail', kwargs={'pk': 55})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        records = Record.objects.all()
        self.assertEqual(len(records), 2)

    def test_record_list(self):
        """
        """
        self.client.login(username='user', password='123')
        url = reverse('record-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['user'], 2)
        self.assertEqual(response.data[1]['user'], 2)
