
from django.conf.urls import include, url
from django.views.generic import TemplateView

from main import routers
from main import views


urlpatterns = [
    url(r'^api/', include(routers.router.urls)),
    url(r'^logout/$', views.logout_view),
    url(r'^signin/$', views.login_view),
    url(r'^signup/$', views.signup_view),
    url(r'^get_profile/$', views.get_profile),
    url(r'^get_summary/$', views.get_summary),
    url(r'^$', TemplateView.as_view(template_name="app.html")),
]
