from main import views
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register("records", views.RecordsViewSet, base_name="record")
router.register("users", views.UsersViewSet)
