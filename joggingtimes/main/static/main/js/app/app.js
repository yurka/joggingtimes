var app = app || {};

(function($){

	app.getCookie = function(name){
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            // Does this cookie string begin with the name we want?
	            if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	}

	$.ajaxSetup({
	    headers: {
	        "X-Csrftoken": app.getCookie('csrftoken')
	    }
	});

	app.conf = {
		apiRoot: '/api',
		profileUrl: "/get_profile/",
	};

	app.state = {
		user: null
	};


	app.Router = Backbone.Router.extend({

	    routes: {
	        "": "records", // #records
	        "signin": "signin", // #signin
	        "signup": "signup", // #signup
	        "users": "users" // #users
	    },

	    signin: function() {
	        app.views.signin.render();
	    },

	    signup: function() {
	        app.views.signup.render();
	    },

	    records: function(query, page) {
	        app.views.status.render();
	        app.views.summary.render();
	        app.views.summary.$el.show();
	        app.views.records.render();
	        app.views.users.$el.hide();
	        app.views.records.$el.show();
	        app.records.fetch({
	            reset: true
	        });
	    },

	    users: function(query, page) {
	        app.views.status.render();
	    	app.views.summary.$el.hide();
	    	app.views.records.$el.hide();
	    	app.views.users.$el.show();
	        app.views.users.render();
	        app.users.fetch({
	            reset: true
	        });
	    },

	    execute: function(callback, args, name) {
	        app.views.signin.$el.empty();
	        if (!app.state.user && name != 'signin' && name != 'signup') {

	            $.get(app.conf.profileUrl, function(resp) {
	                if (resp.username === "") {
	                    app.router.navigate('signin', {
	                        trigger: true
	                    });
	                } else {
	                    app.state.user = resp.username;
	                    app.state.is_staff = resp.is_staff;
	                    if (callback) callback.apply(this, args);
	                }
	            });

	        } else {
	            if (callback) callback.apply(this, args);
	        }
	    }
	});

	app.router = new app.Router(); 
	Backbone.history.start();


})(jQuery);