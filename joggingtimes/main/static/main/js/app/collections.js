var app = app || {};

(function($){

	app.RecordsCollection = Backbone.Collection.extend({
	    url: function() {
	        return app.conf.apiRoot + '/records/';
	    },

	    model: app.Record,
	    comparator: 'date'
	});

	app.records = new app.RecordsCollection();

	app.UsersCollection = Backbone.Collection.extend({
	    url: function() {
	        return app.conf.apiRoot + '/users/';
	    },

	    model: app.User,
	});
	app.users = new app.UsersCollection();

})(jQuery);