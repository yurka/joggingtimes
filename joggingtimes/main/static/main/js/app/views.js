var app = app || {};

(function($){

	app.RecordListView = Backbone.View.extend({
	    el: "#records",
	    cxt: {},

	    events: {
	        "change .datepicker": "applyFilter",
	        "click .clear": "clearFilter",
	        "click .add": "showAddForm",
	    },

	    initialize: function() {
	        this.listenTo(app.records, 'add', this.addAll);
	        this.listenTo(app.records, 'remove', this.addAll);
	        this.listenTo(app.records, "reset", this.addAll);
	        this.listenTo(app.records, "sort", this.addAll);
	    },

	    render: function() {
	        var tpl = _.template($("#records_list_tpl").html());
	        this.$el.html(tpl(this.ctx));


	        $(".datepicker").each(function() {
	            $(this).datepicker({
	                orientation: "top",
	                autoclose: true,
	                format: "yyyy-mm-dd"

	            });
	        });

	        return this;
	    },

	    addOne: function(rec) {
	        var view = new app.RecordView({
	            model: rec
	        });
	        this.$el.find('tbody').append(view.render().el);
	    },

	    // Add all items in the **Todos** collection at once.
	    addAll: function() {
	        this.$el.find('tbody').html('');
	        app.records.each(this.addOne, this);
	    },

	    applyFilter: function() {
	        var data = {
	            date_from: this.$el.find(".date_from").val(),
	            date_to: this.$el.find(".date_to").val(),
	        }

	        app.records.fetch({
	            reset: true,
	            data: $.param(data)
	        });
	    },

	    clearFilter: function() {
	        this.$el.find(".date_from").val("");
	        this.$el.find(".date_to").val("");
	        app.records.fetch({
	            reset: true,
	        });

	    },

	    showAddForm: function(){
	        app.views.form.reset().render().show();

	    },

	    addNewRecord: function(){

	    }

	});


	app.RecordView = Backbone.View.extend({
	    tagName: "tr",
	    className: "record",
	    events: {
	        "click .del": "deleteRecord",
	        "click .edit": "openEditDialog",
	    },
	    cxt: {},
	    initialize: function() {
	        this.listenTo(this.model, "change", this.render);
	    },
	    render: function() {
	        // console.log("REC RENDER", this.model);
	        var tpl = _.template($("#record_tpl").html());
	        this.$el.html(tpl(this.model.toJSON()));
	        return this;
	    },
	    deleteRecord: function(){
	        this.model.destroy();
	    },

	    openEditDialog: function(){
	        var form_data = new app.RecordForm({record: this.model});
	        form_data.set('action', 'Edit record');
	        app.views.form.setData(form_data).render().show();
	    }
	});


	app.SignInView = Backbone.View.extend({
	    el: "#signin",
	    defaultCtx: {message: null},
	    tpl: _.template($("#signin_tpl").html()),
	    events: {
	        "click .icon": "open",
	        "submit .signin-form": "signinUser",
	    },

	    initialize: function() {

	    },

	    render: function(ctx) {
	    	var ctx = ctx || this.defaultCtx;
	        this.$el.html(this.tpl(ctx));
	    },

	    signinUser: function() {

	    	var self = this;
	        var data = {
	            username: $("#username").val(),
	            password: $("#password").val(),
	        }
	        $.post("/signin/", data, function(resp) {
	            if (resp.username) {
	                app.state.user = resp.username;
	                app.state.is_staff = resp.is_staff;
	                app.views.status.render();
	            	$.ajaxSetup({
					    headers: {
					        "X-Csrftoken": app.getCookie('csrftoken')
					    }
					});
	                app.router.navigate("", {
	                    trigger: true
	                });
	            } else {
	                self.render(resp);
	            }
	        });
	        return false;
	    }
	});


	app.SignUpView = Backbone.View.extend({
	    el: "#signin",
	    defaultCtx: {message: null},
	    tpl: _.template($("#signup_tpl").html()),
	    events: {
	        "click .icon": "open",
	        "submit .signup-form": "signupUser",
	    },

	    initialize: function() {

	    },

	    render: function(ctx) {
	    	var ctx = ctx || this.defaultCtx;
	        this.$el.html(this.tpl(ctx));
	    },


	    signupUser: function() {
	    	var self = this;
	        var data = {
	            username: $("#username").val(),
	            password: $("#password").val(),
	            password_repeat: $("#password_repeat").val(),
	        }
	        $.post("/signup/", JSON.stringify(data), function(resp) {
	            if (resp.username) {
	                app.state.user = resp.username;
	                app.router.navigate("signin", {
	                    trigger: true
	                });
	            } else {
	                self.render(resp);
	            }
	        });
	        return false;
	    }
	});


	app.StatusView = Backbone.View.extend({
	    el: "#status",
	    ctx: {},
	    events: {
	        "click #logout": "logout"
	    },

	    initialize: function() {

	    },

	    render: function() {
	        this.ctx['username'] = app.state.user;
	        this.ctx['is_staff'] = app.state.is_staff;
	        var tpl = _.template($("#status_tpl").html());
	        this.$el.html(tpl(this.ctx));
	    },

	    logout: function() {
	        $.ajax({
	            url: "/logout/",
	            method: "POST",
	            headers: {
	                "X-Csrftoken": app.getCookie('csrftoken')
	            },
	            success: function() {
	                location.reload()
	            }
	        });
	    }
	});


	app.SummaryView = Backbone.View.extend({
	    el: "#summary",
	    ctx: {},
	    tpl: _.template($("#summary_tpl").html()),

	    initialize: function() {
	    	this.listenTo(app.records, 'update', this.render);
	    },

	    render: function() {
	    	var self = this;
	    	$.get("/get_summary/", function(resp){
		        self.$el.html(self.tpl(resp));
	    	});
	    },
	});


	app.RecordFromView = Backbone.View.extend({
	    el: ".modal-dialog",
	    tpl: _.template($("#form_tpl").html()),
	    events: {
	        'click .modal-footer .commit': 'commit'
	    },

	    initialize: function(form_data){
    		this.form_data = form_data || new app.RecordForm(); 
	    },

	    setData: function(form_data){
	    	this.form_data = form_data;
	    	return this;
	    },

	    reset: function(){
    		this.form_data = new app.RecordForm();
    		return this; 
	    },

	    render: function(ctx) {
	        var ctx = ctx || this.form_data.toJSON();
	        this.$el.html(this.tpl(ctx));

	        $('#date').datepicker({
	            orientation: "top",
	            autoclose: true,
	            format: "yyyy-mm-dd"
	        });

	        $("#time").mask("00:00:00");

	        return this;
	    },

	    show: function(){
	        $(".modal").modal();
	    },

	    commit: function(){
	        var self = this;
	        if (self.form_data.get('record')){
	        	// do update
		        self.form_data.set('date', $("#date").val());
		        self.form_data.set('distance', $("#distance").val());
		        self.form_data.set('time', $("#time").val());
		        self.form_data.updateRecord();

		        self.form_data.get('record').save([], {
		            success:function(model, resp, options){
		                $(".modal").modal("hide");
		                app.records.sort();
		            }, 
		            error: function(model, resp, options){
		                var errors = _.extend({}, self.form_data.defaultErrors, resp.responseJSON);
		                var form_data = self.form_data;
		                form_data.set('errors', errors);
		                app.views.form.render(form_data.toJSON());
		            }
		        });
	        }else{
	        	// do insert
		        var newRec = new app.Record({
		            date: $("#date").val(),
		            time: $("#time").val(),
		            distance: $("#distance").val(),
		        });

		        newRec.save([], {
		            success:function(model, resp, options){
		                $(".modal").modal("hide");
		                app.records.unshift(model, {trigger: true});
		                app.records.sort();
		            }, 
		            error: function(model, resp, options){
		                var errors = _.extend({}, self.form_data.defaultErrors, resp.responseJSON);
		                var form_data = new app.RecordForm({record: newRec});
		                form_data.set('errors', errors);
		                app.views.form.render(form_data.toJSON());
		            }
		        });
	        }
	        
	    },

	});


	app.UserView = app.RecordView.extend({
        tpl: _.template($("#user_tpl").html()),

	    events: {
	        "click .del": "deleteRecord",
	        "click .toggle_active": "toggleActive",
	        "click .toggle_superuser": "toggleSuperuser",
	    },

	    render: function() {
	        this.$el.html(this.tpl(this.model.toJSON()));
	        return this;
	    },

	    toggleActive: function(){
	    	this.model.save(
	    		{is_active: !this.model.get('is_active')},
	    		{patch:true,
	    		 success: function(m, r){
	    			this.model.set('is_active', r.is_active);
	    		}});
	    },

	    toggleSuperuser: function(){
	    	this.model.save(
	    		{is_staff: !this.model.get('is_staff')},
	    		{patch:true,
	    		 success: function(m, r){
	    			this.model.set('is_staff', r.is_staff);
	    		}});
	    }

	});


	app.UserListView = Backbone.View.extend({
	    el: "#users",
        tpl: _.template($("#users_list_tpl").html()),
	    cxt: {},

	    events: {
	        "click .add": "showAddForm",
	    },

	    initialize: function() {
	        this.listenTo(app.users, 'add', this.addAll);
	        this.listenTo(app.users, 'remove', this.addAll);
	        this.listenTo(app.users, "reset", this.addAll);
	    },

	    render: function() {
	        this.$el.html(this.tpl(this.ctx));
	        return this;
	    },

	    addOne: function(user) {
	        var view = new app.UserView({
	            model: user
	        });
	        this.$el.find('tbody').append(view.render().el);
	    },

	    // Add all items in the **Todos** collection at once.
	    addAll: function() {
	        this.$el.find('tbody').html('');
	        app.users.each(this.addOne, this);
	    },

	    showAddForm: function(){
	        app.views.userform.reset().render().show();
	    },
	});


	app.views = {
	    signin: new app.SignInView(),
	    signup: new app.SignUpView(),
	    records: new app.RecordListView(),
	    status: new app.StatusView(),
	    form: new app.RecordFromView(),
	    summary: new app.SummaryView(),
	    users: new app.UserListView(),
	}

})(jQuery);