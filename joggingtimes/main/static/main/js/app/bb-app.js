function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


$.ajaxSetup({
    headers: {
        "X-Csrftoken": getCookie('csrftoken')
    }
});

var Config = {
    apiRoot: "/api",
}

var State = {
    user: null
}

var Record = Backbone.Model.extend({
    // url: "/api/records/",
    urlRoot: '/api/records/',
    ping: function() {
        console.log("pong");
    }
});

var RecordsCollection = Backbone.Collection.extend({
    url: function() {
        return Config.apiRoot + '/records/';
    },

    model: Record
});

var Records = new RecordsCollection;

var RecordListView = Backbone.View.extend({
    el: "#records",
    cxt: {},

    events: {
        "change .datepicker": "applyFilter",
        "click .clear": "clearFilter",
        "click .add": "showAddForm",
    },

    initialize: function() {
        this.listenTo(Records, 'add', this.addAll);
        this.listenTo(Records, 'remove', this.addAll);
        // this.listenTo(this.model, "change", this.render);
        this.listenTo(Records, "reset", this.addAll);
    },

    render: function() {
        console.log("LIST RENDER");
        var tpl = _.template($("#records_list_tpl").html());
        this.$el.html(tpl(this.ctx));


        $(".datepicker").each(function() {
            $(this).datepicker({
                orientation: "top",
                autoclose: true,
                format: "yyyy-mm-dd"

            });
        });

        return this;
    },

    addOne: function(rec) {
        var view = new RecordView({
            model: rec
        });
        this.$el.find('.table').append(view.render().el);
    },

    // Add all items in the **Todos** collection at once.
    addAll: function() {
        this.$el.find('.table').html('');
        Records.each(this.addOne, this);
    },

    applyFilter: function() {
        var data = {
            date_from: this.$el.find(".date_from").val(),
            date_to: this.$el.find(".date_to").val(),
        }

        Records.fetch({
            reset: true,
            data: $.param(data)
        });
    },

    clearFilter: function() {
        this.$el.find(".date_from").val("");
        this.$el.find(".date_to").val("");
        Records.fetch({
            reset: true,
        });

    },

    showAddForm: function(){
      Views.form.render(new RecordForm().toJSON()).show();

    },

    addNewRecord: function(){

    }

});


var RecordView = Backbone.View.extend({
    tagName: "tr",
    className: "record",
    events: {
        "click .del": "deleteRecord",
        "click .edit": "openEditDialog",
    },
    cxt: {},
    initialize: function() {
        this.listenTo(this.model, "change", this.render);
    },
    render: function() {
        console.log("REC RENDER", this.model);
        var tpl = _.template($("#record_tpl").html());
        this.$el.html(tpl(this.model.toJSON()));
        return this;
    },
    deleteRecord: function(){
        this.model.destroy();
    },

    openEditDialog: function(){
        var form_data = _.extend({}, Views.form.model.toJSON(), this.model.toJSON());
        form_data.action = 'Edit record';
        Views.form.render(form_data).show();
    }
});


var SigninView = Backbone.View.extend({
    el: "#signin",
    cxt: {},
    events: {
        "click .icon": "open",
        "submit .form-signin": "signinUser",
    },

    initialize: function() {

    },

    render: function() {
        var tpl = _.template($("#signin_tpl").html());
        this.$el.html(tpl(this.ctx));
    },

    signinUser: function() {
        var data = {
            username: $("#username").val(),
            password: $("#password").val(),
        }
        $.post("/signin/", data, function(resp) {
            if (resp.username) {
                State.user = resp.username;
                console.log("SIGN!", State.user);
                app.navigate("", {
                    trigger: true
                });
            } else {
                showMessage(resp.message);
            }
        });
        return false;
    }
});


var StatusView = Backbone.View.extend({
    el: "#status",
    ctx: {},
    events: {
        "click #logout": "logout"
    },
    initialize: function() {
        console.log('STATUS INIT', this.ctx);
    },
    render: function() {
        this.ctx['username'] = State.user;
        var tpl = _.template($("#status_tpl").html());
        this.$el.html(tpl(this.ctx));
        console.log('STATUS RENDER', this.ctx.username);
    },
    logout: function() {
        $.ajax({
            url: "/logout/",
            method: "POST",
            headers: {
                "X-Csrftoken": getCookie('csrftoken')
            },
            success: function() {
                location.reload()
            }
        });
    }
});


var RecordForm = Backbone.Model.extend({
    defaults: function(){
        return {
            date: new Date().toISOString().substr(0,10),
            time: '',
            distance: '',
            errors: _.clone(this.defaultErrors),
            action: 'Add new record'
        }
    },

    defaultErrors:  {
                date: [],
                time: [],
                distance: [],
            },
});

var RecordFromView = Backbone.View.extend({
    el: ".modal-dialog",
    tpl: _.template($("#form_tpl").html()),
    model: new RecordForm(),
    events: {
        'click .modal-footer .commit': 'commit'
    },
    render: function(ctx) {
        var ctx = ctx || {};
        this.$el.html(this.tpl(ctx));

        $('#date').datepicker({
            orientation: "top",
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $("#time").mask("00:00:00");

        console.log('FORM RENDER', ctx);
        return this;
    },

    show: function(){
        $(".modal").modal();
    },

    commit: function(){
        var self = this;
        var newRec = new Record({
            date: $("#date").val(),
            time: $("#time").val(),
            distance: $("#distance").val(),
        });
        newRec.save([], {
            success:function(model, resp, options){
                $(".modal").modal("hide");
                Records.unshift(model, {trigger: true});
                console.log("RECORD ADDED", Records, model);
            }, 
            error: function(model, resp, options){
                var errors = _.extend({}, self.model.defaultErrors, resp.responseJSON);
                model.set('errors', errors);
                console.log(model, errors);
                Views.form.render(model.toJSON());
            }
        });
        
    },

});


Views = {
    signin: new SigninView(),
    records: new RecordListView(),
    status: new StatusView(),
    form: new RecordFromView(),
}


var App = Backbone.Router.extend({
    routes: {
        "": "records", // #records
        "signin": "signin", // #signin
        "users": "users" // #users
    },
    signin: function() {
        Views.signin.render();
    },
    records: function(query, page) {
        Views.status.render();
        Views.records.render();
        Records.fetch({
            reset: true
        });
    },
    users: function(query, page) {
        console.log("route users");
    },

    execute: function(callback, args, name) {
        console.log("sss", callback, args, name);
        Views.signin.$el.empty();
        if (!State.user && name != 'signin') {

            $.get("/get_profile/", function(resp) {
                if (resp.username === "") {
                    console.log("REDIR");
                    app.navigate('signin', {
                        trigger: true
                    });
                } else {
                    console.log("SETUSER");
                    State.user = resp.username;
                    if (callback) callback.apply(this, args);
                }
            });

        } else {
            if (callback) callback.apply(this, args);
        }
    }
});

var app = new App(); // Создаём контроллер

Backbone.history.start(); // Запускаем HTML5 History push
