var app = app || {};

(function($){

	app.Record = Backbone.Model.extend({
	    urlRoot: '/api/records/',
	});


	app.RecordForm = Backbone.Model.extend({
	    defaults: function(){
	        return {
	            date: new Date().toISOString().substr(0, 10),
	            time: '',
	            distance: '',
	            errors: _.clone(this.defaultErrors),
	            action: 'Add new record'
	        }
	    },

	    defaultErrors:  {
	                date: [],
	                time: [],
	                distance: [],
	            },

	    initialize: function(attrs){
	    	var attrs = attrs || {};
	    	var rec = attrs.record;
	    	if(rec){
	    		_.extend(this.attributes, rec.attributes);
		    	this.listenTo(this, "change", this.updateRecord);
		    	this.attributes.record = rec;
	    	}else{
		    	this.attributes.record = null;
	    	}
	    },

	    updateRecord: function(){
	    	var rec = this.get('record');
	    	if(!rec){
	    		return;
	    	}

	    	rec.set({
	    		date: this.get("date"),
	    		time: this.get("time"),
	    		distance: this.get("distance"),
	    	})

	    }
	});


	app.User = Backbone.Model.extend({
	    urlRoot: '/api/users/',
	});


	// app.UserForm = Backbone.Model.extend({
	//     defaults: function(){
	//         return {
	//             is_staff: false,
	//             is_active: true,
	//             errors: _.clone(this.defaultErrors),
	//             action: 'Add new user'
	//         }
	//     },

	//     defaultErrors:  {
	//                 username: [],
	//                 password: [],
	//                 password_repeat: [],
	//             },

	//     initialize: function(attrs){
	//     	var attrs = attrs || {};
	//     	var rec = attrs.record;
	//     	if(rec){
	//     		_.extend(this.attributes, rec.attributes);
	// 	    	this.listenTo(this, "change", this.updateRecord);
	// 	    	this.attributes.record = rec;
	//     	}else{
	// 	    	this.attributes.record = null;
	//     	}
	//     },

	//     updateRecord: function(){
	//     	var rec = this.get('record');
	//     	if(!rec){
	//     		return;
	//     	}

	//     	rec.set({
	//     		username: this.get("date"),
	//     		time: this.get("time"),
	//     		distance: this.get("distance"),
	//     	})

	//     }
	// });

})(jQuery);