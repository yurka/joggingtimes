import datetime
import json
from rest_framework import viewsets, permissions, status
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from django.contrib.auth import authenticate, login, logout

from main.models import Record, User
from main.serializers import (RecordSerializer,
                              UserSerializer,
                              UserCreateSerializer)


DATE_FORMAT = "%Y-%m-%d"


class IsOwnerOrAdmin(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user

    def has_object_permission(self, request, view, obj):
        return request.user.is_staff or request.user == obj.user


class RecordsViewSet(viewsets.ModelViewSet):

    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    serializer_class = RecordSerializer
    permission_classes = (permissions.IsAuthenticated,
                          IsOwnerOrAdmin)

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        user = self.request.user
        qs = Record.objects.filter(user=user)
        qs = self._filter_by_date(qs)
        return qs

    def _filter_by_date(self, queryset):
        """
        Filters queryset by date, if provided
        """
        date_from = self.request.query_params.get('date_from')
        date_to = self.request.query_params.get('date_to')
        if date_from is None and date_to is None:
            return queryset
        try:
            if date_from:
                date_from = datetime.datetime.strptime(date_from, DATE_FORMAT)
                queryset = queryset.filter(date__gte=date_from)
            if date_to:
                date_to = datetime.datetime.strptime(date_to, DATE_FORMAT)
                queryset = queryset.filter(date__lte=date_to)
            return queryset
        except ValueError:
            raise ParseError(
                "Invalid date: must match format %s" % DATE_FORMAT)


class UsersViewSet(viewsets.ModelViewSet):
    """
    """
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAdminUser,)
    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = UserCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=headers)


@api_view(['POST'])
def login_view(request):

    username = request.POST.get('username')
    password = request.POST.get('password')
    if not username and not password:
        return Response({"message": "Provide your username and password"})
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return Response({"username": username,
                             "is_staff": user.is_staff})
        return Response({"message": "Account disabled"})
    return Response({"message": "Invalid credentials"})


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def logout_view(request):
    logout(request)
    return Response({"message": "bye!"})


@api_view(['GET'])
def get_profile(request):
    if request.user.is_authenticated:
        data = {"username": request.user.username,
                "is_staff": request.user.is_staff}
    else:
        data = {"username": None,
                "is_staff": False}
    return Response(data)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated,))
def get_summary(request):
    data = Record.get_summary(request.user)
    return Response(data)


@api_view(['POST'])
def signup_view(request):
    data = json.loads(request.body)
    serializer = UserCreateSerializer(data=data)
    if serializer.is_valid():
        user = serializer.save()
        return Response({"username": user.username})
    else:
        return Response({
            "message": serializer.errors.pop('non_field_errors', None),
            "errors":  serializer.errors
        })
