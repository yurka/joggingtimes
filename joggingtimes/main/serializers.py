from django.contrib.auth.models import User
from rest_framework import serializers

from main.models import Record


class RecordSerializer(serializers.ModelSerializer):

    avg_speed = serializers.FloatField(read_only=True)

    class Meta:
        model = Record
        read_only_fields = ['user', 'avg_speed']

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super(RecordSerializer, self).create(validated_data)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'is_staff', 'is_active')


class UserCreateSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=30)
    password = serializers.CharField(max_length=50, write_only=True)
    password_repeat = serializers.CharField(max_length=50, write_only=True)

    def create(self, validated_data):
        return User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
        )

    def validate(self, data):
        """
        Check password and username
        """
        if User.objects.filter(username=data['username']).exists():
            raise serializers.ValidationError(
                "User with name '{}' already exists".format(data['username']))
        if data['password'] != data['password_repeat']:
            raise serializers.ValidationError("Password mismatch")
        return data
